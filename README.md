## Crude text-only terminal

**simpleTerminal.X** provides a crude, 9600 baud, text-only terminal.
There are only three special characters recognized:

* 0x0d - move (invisible) cursor to the beginning of the next line
* 0x0a - Move the cursor down one line
* 0x0c - Clear the screen and set the cursor to the top left

Text wraps at the right of the screen and at the bottom of the screen.
There is no scrolling.  Text is displayed in _GOLD_.  If a framing error
should occur, the words `Framing error` will appear in _HOTPINK_.  If an
UART overrun should occur, the words `Overrun error` will appear in
_TOMATO_.  Either of these is unlikely as the UART has a 4 character
FIFO, the application a 1024 character buffer, and the characters are
received on interrupt which is never turned off.  However, a buffer
wrap is possible.