/*! \file  U2RXISR.c
 *
 *  \brief U2RX interrupt handler
 *
 *  UART2Handler() is entered on a receive interrupt on
 *  UART2.  The handler clears the interrupt flag, places the
 *  character in the buffer, and increments the buffer pointer.
 *  If the pointer runs past the end of the buffer the pointer
 *  is wrapped.
 *
 *  \author jjmcd
 *  \date March 10, 2014, 12:02 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 */

#include <xc.h>
#include <sys/attribs.h>
#include "simpleTerminal.h"

/*! UART2Handler - Receive a character */
/*! UART2Handler() is entered on a receive interrupt on
 *  UART2.  The handler clears the interrupt flag, places the
 *  character in the buffer, and increments the buffer pointer.
 *  If the pointer runs past the end of the buffer the pointer
 *  is wrapped.
 */
void __ISR(_UART_2_VECTOR, IPL4SOFT) UART2Handler(void)
{
    IFS1bits.U2RXIF = 0; // Clear UART 2 recieve interrupt flag

    /* If there is actually a character available */
    if (U2STAbits.URXDA)
    {
        /* Place the character in the buffer */
        *pNextIn = U2RXREG;
        /* Bump the buffer pointer */
        pNextIn++;
        /* Wrap if past the end of the buffer */
        if (pNextIn > pEndBuffer)
            pNextIn = szBuffer;
    }
}
