/*! \file  showChar.c
 *
 *  \brief Display a character on the TFT
 *
 *  showChar() displays the provided character on the TFT in the
 *  location determined by the gllobals nScreenX and nScreenY.
 *  Carriage return and line feed are handled, as well as a
 *  control-L for screen clear.
 *
 *  \author jjmcd
 *  \date March 10, 2014, 12:41 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 */
#include <xc.h>
#include "../include/TFT.h"
#include "simpleTerminal.h"

/*! showChar - Display a character on the TFT */
/*! showChar() displays the provided character on the TFT in the
 *  location determined by the gllobals nScreenX and nScreenY.
 *  Carriage return and line feed are handled, as well as a
 *  control-L for screen clear.
 *
 *  \param ch - byte - character to display
 */
void showChar( uint8_t ch )
{
  int ybottom;

  /* Is it a control character? */
  if ( ch < ' ' )
    {
      switch ( ch )
        {
	case 0x0c:                      /* Screen clear */
	  TFTclear();
	  nScreenX=nScreenY=0;
	  break;
	case 0x0d:                      /* Carriage return */
	  nScreenX=0;
          /* Calculate the extent of the next line */
	  ybottom = nScreenY+2*YDELTA;
	  if ( ybottom>239 )
	    ybottom = 239;
          /* And clear the next line */
	  TFTclearRect(0,0,0,0,nScreenY+YDELTA,319,ybottom);
	case 0x0a:                      /* Line feed */
	  nScreenY += YDELTA;
	  if ( nScreenY>230 )
	    nScreenY = 0;
	  /* With no linefeed delay, every line starts with a missed character */
	  delay(100); // at 150, occasional miss, even 200, no worse at 100
	  break;
	default:                        /* Others, ignore */
	  break;
        }
    }
  /* If it is a printable character */
  else
    {
      /* Display the character */
      TFTprintChar( ch, nScreenX, nScreenY );
      /* Move to the next horizontal position */
      nScreenX += XDELTA;
      /* If past the right, wrap */
      if ( nScreenX>310 )
	nScreenX = 0;
    }
}
