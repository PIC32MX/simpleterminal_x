/*! \file  simpleTerminal.c
 *
 *  \brief Simple terminal with echo
 *
 *  Application provides a simple terminal with echo.  Characters
 *  received on the serial port are displayed on the TFT and
 *  echoed out the serial port.  If a Control-L (0x0c) is
 *  received, the screen is cleared.  If a line feed (0x0a)
 *  is received, the curcor is moved to the next line. If a
 *  carriage return (0x0d) is received, the cursor is moved to
 *  the beginning of the NEXT line.
 *
 *  \author jjmcd
 *  \date March 1, 2014, 1:48 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 */

#include <xc.h>
#include <sys/attribs.h>
#include "../include/ConfigBits.h"
#include "../include/TFT.h"
#include "../include/colors.h"
#define EXTERN
#include "simpleTerminal.h"

/*! main - Simple terminal*/
/*! Application provides a simple terminal with echo.  Characters
 *  received on the serial port are displayed on the TFT and
 *  echoed out the serial port.  If a Control-L (0x0c) is
 *  received, the screen is cleared.  If a line feed (0x0a)
 *  is received, the curcor is moved to the next line. If a
 *  carriage return (0x0d) is received, the cursor is moved to
 *  the beginning of the NEXT line.  Whenever the cursor is moved
 *  to the next line, the next line is cleared.
 *
 *  If the line exceeds the width of the screen, the characters
 *  wrap, but the line is not incremented.  When moving past the
 *  bottom of the screen, the position wraps to the top.
 */
int main( void )
{
    /* Initialize UART2 */
    setupUART();

    /* Initialize the TFT and clear */
    SetupHardware();
    TFTinit( LANDSCAPE );
    TFTsetBackColorX(BLACK);
    TFTsetColorX(TEXTCOLOR);
    TFTclear();

    /* LEDs */
    _TRISB7 = 0;
    _TRISB5 = 0;
    _LATB5 = 1;
    _LATB7 = 1;

    /* Start at the upper left of the TFT */
    nScreenX = nScreenY = 0;

    /* Select the font and colors, clear the TFT */
    TFTsetFont((fontdatatype*)SmallFont);
    TFTsetColorX(TEXTCOLOR);
    TFTsetBackColorX(BLACK);
    TFTclear();

    /* Initialize the buffer pointers */
    pNextIn = szBuffer;
    pNextOut = szBuffer;
    pEndBuffer = szBuffer+1023;

    IEC1bits.U2RXIE   = 1;   /* Enable receive interrupt */
    INTEnableSystemMultiVectoredInt();
    
    while (1)
    {
        /* If the in and out pointers are different, char is available */
        if ( pNextOut != pNextIn )
        {
            /* Turn on LED if transmit buffer bottled up */
            while ( U2STAbits.UTXBF )
                _LATB5=0;
            /* Move the character to the buffer */
            while ( U2STAbits.UTXBF )
                ;
            U2TXREG = *pNextOut;
            /* If it was a carriage return, provide a linefeed */
            if ( *pNextOut == 0x0d )
            {
                while ( U2STAbits.UTXBF )
                    ;
                U2TXREG = 0x0a;
            }
            /* Clear the LED */
            _LATB5=1;
            /* Set the text color */
            TFTsetColorX(TEXTCOLOR);
            /* Display the character */
            showChar( *pNextOut );
            /* Move to the next character */
            pNextOut++;
            /* Wrap if at end of buffer */
            if ( pNextOut > pEndBuffer )
                pNextOut = szBuffer;
            /* Check for framing error */
            if ( U2STAbits.FERR )
            {
                TFTsetColorX(HOTPINK);
                TFTprint("  Framing error ",RIGHT,10,0);
                TFTsetColorX(TEXTCOLOR);
                U2STAbits.FERR = 0;
            }
            /* Check for overrun error */
            if ( U2STAbits.OERR )
            {
                TFTsetColorX(TOMATO);
                TFTprint("  Overrun error ",RIGHT,20,0);
                TFTsetColorX(TEXTCOLOR);
                U2STAbits.OERR = 0;
            }
        }
    }
    return 0;
}
