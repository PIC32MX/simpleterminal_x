/*! \file  simpleTerminal.h
 *
 *  \brief Shared definitions for simple terminal
 *
 *  \author jjmcd
 *  \date March 10, 2014, 12:02 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 */

#ifndef SIMPLETERMINAL_H
#define	SIMPLETERMINAL_H

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef EXTERN
#define EXTERN extern
#endif

/*! Color in which to display normal characters */
#define TEXTCOLOR GOLD
/*! Width of a displayed character */
#define XDELTA 8
/*! Height of a displayed character */
#define YDELTA 12

/*! Horizontal position of the next character on the screen */
EXTERN int nScreenX;
/*! Vertical position of the next character on the screen */
EXTERN int nScreenY;
/*! Buffer for incoming characters */
EXTERN char szBuffer[1024];
/*! Position to place the next character in the buffer */
EXTERN char *pNextIn;
/*! Position in the buffer of the next character to process */
EXTERN char *pNextOut;
/*! Pointer to the end of the buffer */
EXTERN char *pEndBuffer;

#ifdef	__cplusplus
}
#endif

#endif	/* SIMPLETERMINAL_H */

